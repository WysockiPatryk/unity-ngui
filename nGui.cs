﻿using UnityEngine;
using System.Collections;

public class nGui : MonoBehaviour {

	private float sliderVal = 0.0f;
	private Vector2 scrollPosition = Vector2.zero;

	public int selGridInt = 0;
  public string[] selStrings = new string[] {"button 1", "button 2", "button 3", "button 4"};

	void OnGUI()
	{
		scrollPosition = GUI.BeginScrollView(new Rect(0, 0, 200, 200), scrollPosition, new Rect(0, 0, 400, 600));

		GUI.Box(new Rect(0, 0, 200, 300), "group is here");
		sliderVal = GUI.HorizontalSlider(new Rect(0, 80, 200, 20), sliderVal, 0.0f, 10.0f);
		GUI.Label(new Rect(0, 60, 200, 20), "slider value = " + sliderVal.ToString());

		GUI.Label(new Rect(0, 100, 200, 20), selStrings[selGridInt].ToString());
		selGridInt = GUI.SelectionGrid(new Rect(0, 130, 200, 50), selGridInt, selStrings, 2);

		GUI.EndScrollView();
	}

}
